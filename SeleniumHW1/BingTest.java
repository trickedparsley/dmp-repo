import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Keys;

public class BingTest {
    @Test
    public void browseWithBing(){
        System.setProperty("webdriver.chrome.driver", "D:\\Projects\\Selenium\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();

        webDriver.get("https://www.bing.com");

        webDriver.findElement(By.xpath("//input[@id='sb_form_q']")).sendKeys("Telerik Academy Alpha");
        webDriver.findElement(By.id("search_icon")).click();

        webDriver.findElement(By.xpath("//h2[@class=' b_topTitle']")).click();

        String title = webDriver.getTitle();

        Assert.assertTrue("Incorrect first result.", title.matches("IT Career Start in 6 Months - Telerik Academy Alpha"));

        webDriver.quit();
    }
}
