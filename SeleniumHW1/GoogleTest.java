import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {
    @Test
    public void browseWithGoogle(){
        System.setProperty("webdriver.chrome.driver", "D:\\Projects\\Selenium\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();

        webDriver.get("https://www.google.com");

        webDriver.findElement(By.xpath("//div[@class=\"QS5gu sy4vM\"]")).click();

        webDriver.findElement(By.xpath("//input[@class=\"gLFyf gsfi\"]")).sendKeys("Telerik Academy Alpha", Keys.RETURN);

        webDriver.findElement(By.xpath("//h3[@class='LC20lb MBeuO DKV0Md']")).click();

        String title = webDriver.getTitle();

        Assert.assertTrue("Incorrect first result.", title.matches("IT Career Start in 6 Months - Telerik Academy Alpha"));

        webDriver.quit();
    }
}
